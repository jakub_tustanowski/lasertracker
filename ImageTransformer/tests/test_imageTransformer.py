from unittest import TestCase
import cv2

from ImageTransformer.ImageTransformer import ImageTransformer
from config.Configuration import config

class TestImageTransformer(TestCase):
    def test_transform(self):
        img = cv2.imread(config.parser["PATHS"]["project_root"]+config.parser["PATHS"]["images_rel_path"] + "5.jpg")
        transformer = ImageTransformer()
        result = transformer.transform(img)

        cv2.imshow('transformed image', result)
        cv2.waitKey(0)
        cv2.destroyWindow('transformed image')

