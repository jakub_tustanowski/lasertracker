**1. Kalibracja**

* Podaj rozdzielczość ekranu
* Skieruj laser na rogi ekranu

**2. Działanie**

* Na podstawie zapamiętanych danych prostujemy i obcinamy obraz
* Znajdujemy najjaśniejszy punkt przetworzonego obrazu
* Dzięki wcześniejszej transformacji współrzędne punktu na obrazie są równe współrzędnym na ekranie
* Przesuwamy kursor w odpowiednie miejsce