# import the necessary packages
from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
 
# initialize the camera and grab a reference to the raw camera capture
camera = PiCamera()
camera.resolution = (640, 480)
camera.framerate = 32
rawCapture = PiRGBArray(camera, size=(640, 480))
radius = 11
 
# allow the camera to warmup
time.sleep(0.1)


channels = {
            'hue': None,
            'saturation': None,
            'value': None,
            'laser': None,
        }

hue_min = 20
hue_max = 160
sat_min = 100
sat_max = 255
val_min = 200
val_max = 256
 
def threshold_image(channel):
    if channel == "hue":
            minimum = hue_min
            maximum = hue_max
    elif channel == "saturation":
            minimum = sat_min
            maximum = sat_max
    elif channel == "value":
            minimum = val_min
            maximum = val_max

    (t, tmp) = cv2.threshold(
            channels[channel],  # src
            maximum,  # threshold value
            0,  # we dont care because of the selected type
            cv2.THRESH_TOZERO_INV  # t type
    )

    (t, channels[channel]) = cv2.threshold(
            tmp,  # src
            minimum,  # threshold value
            255,  # maxvalue
            cv2.THRESH_BINARY  # type
    )

    if channel == 'hue':
            # only works for filtering red color because the range for the hue
            # is split
         channels['hue'] = cv2.bitwise_not(channels['hue'])

 
# capture frames from the camera
for frame in camera.capture_continuous(rawCapture, format="bgr", use_video_port=True):
	# grab the raw NumPy array representing the image, then initialize the timestamp
	# and occupied/unoccupied text
    image = frame.array
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    channels['hue'], channels['saturation'], channels['value'] = cv2.split(hsv)
        # Threshold ranges of HSV components; storing the results in place
    threshold_image("hue")
    threshold_image("saturation")
    threshold_image("value")

        # Perform an AND on HSV components to identify the laser!
    channels['laser'] = cv2.bitwise_and(
            channels['hue'],
            channels['value']
        )
    channels['laser'] = cv2.bitwise_and(
            channels['saturation'],
            channels['laser']
    )

        # Merge the HSV components back together.
    hsv_image = cv2.merge([
            channels['hue'],
            channels['saturation'],
            channels['value'],
    ])

 
	# show the frame
    gray = cv2.GaussianBlur(gray, (radius, radius), 0)
    (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)
    cv2.circle(image, maxLoc, radius, (255, 0, 0), 2)
    cv2.imshow("Frame", channels['laser'])
    key = cv2.waitKey(1) & 0xFF
 
	# clear the stream in preparation for the next frame
    rawCapture.truncate(0)
 
	# if the `q` key was pressed, break from the loop
    if key == ord("q"):
        break



