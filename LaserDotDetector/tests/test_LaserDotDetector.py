from LaserDotDetector import LaserDotDetector as ldd
from unittest import TestCase
import numpy as np
import cv2
from config.Configuration import config

PROJECT_ROOT = config.parser["PATHS"]["project_root"]
IMAGES_PATH = config.parser["PATHS"]["images_rel_path"]

class TestLaserDotDetector(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.approximateLaserPositions = np.array([
            #          Top-left                Bottom-right
            np.array([[296, 308], [296, 319], [306, 319], [306, 308]]),
            np.array([[200, 225], [200, 230], [205, 230], [205, 225]]),
            np.array([[301, 229], [301, 235], [307, 235], [307, 229]]),
            np.array([[391, 282], [391, 287], [400, 287], [400, 282]]),
            np.array([[586, 323], [586, 328], [590, 328], [590, 323]])
        ])
        cls.imageNames = ['\\' + str(x+1) + '.jpg' for x in range(5)]
        cls.imageSize = (800, 600)
        cls.detector = ldd.LaserDotDetector()

    def test_detect(self):
        for i in range(len(self.imageNames)):
            img = cv2.imread(PROJECT_ROOT + IMAGES_PATH + self.imageNames[i])
            expectedImg = np.zeros((self.imageSize[1], self.imageSize[0]), np.uint8)
            cv2.fillConvexPoly(expectedImg, self.approximateLaserPositions[i], (255, 255, 255))
            # Resizing the image affects dot detection. Adjust the detector parameters when changing the size
            img = cv2.resize(img, self.imageSize)

            imgRed = self.detector.detect(img)
            intersectionPercentage = self.getIntersectionPercentage(expectedImg, imgRed)

            # cv2.imshow('filtered red image', imgRed)
            # cv2.imshow('test image', img)
            # cv2.waitKey(0)
            # cv2.destroyWindow('filtered red image')
            # cv2.destroyWindow('test image')

            self.assertAlmostEqual(1.0, intersectionPercentage, delta=0.1,
                                   msg="Testing image number {}. Intersection percentage: {}"
                                        .format(i+1, intersectionPercentage))

    def getIntersectionPercentage(self, expected, actual):
        result = cv2.bitwise_and(expected, actual)
        resultCount = cv2.countNonZero(result)
        actualCount = cv2.countNonZero(actual)
        return resultCount/actualCount