import configparser as cp
import os

class Configuration(object):

    def __init__(self, filename):
        self.parser = cp.ConfigParser()
        self.parser.optionxform = str
        isFound = self.parser.read(filename)
        self.screenRes(filename)
        if not isFound:
            raise ValueError("Could not find configuration file: {}".format(filename))
        # for name in self.parser.sections():
        #     self.__dict__.update(self.parser.items(name))
        #     print(name)

    def screenRes(self, filename):
        x = 0
        y = 0
        if os.name == 'nt':
            x, y = self.screenResWindows(filename)
        else:
            x, y = self.screenResLinux(filename)
        self.parser.set("RESOLUTION", "x", str(x))
        self.parser.set("RESOLUTION", "y", str(y))
        with open(filename, 'w+') as configfile:
            self.parser.write(configfile)
        return x, y



    def screenResWindows(self, filename):
        import ctypes
        user32 = ctypes.windll.user32
        return user32.GetSystemMetrics(0), user32.GetSystemMetrics(1)

    def screenResLinux(self, filename):
        import subprocess
        cmd = ['xrandr']
        cmd2 = ['grep', '*']
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        p2 = subprocess.Popen(cmd2, stdin = p.stdout, stdout=subprocess.PIPE)
        p.stdout.close()
        resolution_string, _ = p2.communicate()
        resolution = resolution_string.split()[0]
        w,h = resolution.split(b'x')
        return w,h



config = Configuration(os.path.dirname(__file__) + '/config.ini')

